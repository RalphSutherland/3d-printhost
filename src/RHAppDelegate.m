/*
 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "RHAppDelegate.h"
#import "RHLogger.h"
#import "PrinterConfiguration.h"
#import "PrinterConnection.h"
#import "IntegerTransformer.h"
#import "ThreeDConfig.h"
#import "Commands.h"
#import "ThreeDContainer.h"
#import "STL.h"
#import "GCodeVisual.h"
#import "RHActionTabDelegate.h"
#import "RHAnimation.h"
#import "EEPROMController.h"
#import "ThreeDView.h"
#import "GCodeEditorController.h"
#import "LogSplitViewDelegate.h"
#import "ThreeDConfig.h"
#import "GCodeView.h"
#import "SlicerConfig.h"
#import "STLComposer.h"
#import "GCodeView.h"
#import "RHSound.h"

@implementation RHAppDelegate
@synthesize toolbarConnect;
@synthesize window;

#pragma mark Initalization related

-(instancetype)init {
    if((self=[super init])) {
        //[rhlog setView:logView];
        // create an autoreleased instance of our value transformer
        IntegerTransformer *intTrans;
        intTrans = [[[IntegerTransformer alloc] init]
                    autorelease];
        // register it with the name that we refer to it with
        [NSValueTransformer setValueTransformer:intTrans
                                        forName:@"IntegerTransformer"];
        commands = [Commands new];
        [commands readFirmware:@"default" language:@"en"];
    /*    IntegerTransformer *intTrans;
        
        // create an autoreleased instance of our value transformer
        intTrans = [[[IntegerTransformer alloc] init]
                           autorelease];
        
        // register it with the name that we refer to it with
        [NSValueTransformer setValueTransformer:intTrans
                                        forName:@"IntegerTransformer"];
        // Set default values
        [PrinterConfiguration initPrinter];
        conf3d = [ThreeDConfig new];*/
        eepromController = nil;
        sdcardManager = nil;
        codePreview = [ThreeDContainer new];
        stlView = [ThreeDContainer new];
        printPreview = [ThreeDContainer new];
        codeVisual = [[GCodeVisual alloc] init];
        [codePreview->models addLast:codeVisual];
        [codeVisual release];
        printVisual = [[GCodeVisual alloc] initWithAnalyzer:connection->analyzer];
        printVisual->liveView = YES;
        [printPreview->models addLast:printVisual];
    }
    app = self;
    return self;
}
- (void)dealloc {
    [codeVisual release];
    [printVisual release];
    [connectImage release];
    [disconnectImage release];
    [runJobIcon release];
    [pauseJobIcon release];
    [hideFilamentIcon release];
    [viewFilamentIcon release];
    [openPanel release];
    [pausePanel release];
    [super dealloc];
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{    
    connectImage = [[NSImage imageNamed:@"connect"] retain];
    disconnectImage = [[NSImage imageNamed:@"disconnect"] retain];
    runJobIcon = [[NSImage imageNamed:@"runjob"] retain];
    pauseJobIcon = [[NSImage imageNamed:@"pauseicon"] retain];
    viewFilamentIcon = [[NSImage imageNamed:@"preview"] retain];
    hideFilamentIcon = [[NSImage imageNamed:@"previewoff"] retain];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectionOpened:)
                                                 name:@"RHConnectionOpen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectionClosed:)
                                                 name:@"RHConnectionClosed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(jobChanged:)
                                                 name:@"RHJobChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(printerInfoReceived:)
                                                 name:@"RHPrinterInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(temperatureRead:)
                                                 name:@"RHTemperatureRead" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(progressReceived:)
                                                 name:@"RHProgress" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(replaceGCodeView:)
                                                 name:@"RHReplaceGCodeView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(firmwareDetected:)
                                                     name:@"RHFirmware" object:nil];





    [actionTabDelegate tabView:nil didSelectTabViewItem:composerTab];
    [leftTabView selectTabViewItemAtIndex:0];
    [rightTabView selectTabViewItemAtIndex:0];
    [self updateJobButtons];
    [self updateViewFilament];
    [rhlog refillView];
    openPanel = [[NSOpenPanel openPanel] retain];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setAllowsMultipleSelection:NO];
    openPanel.message = NSLocalizedString(@"Load G-Code file",@"Load G-Code file");
    pausePanel = [[NSAlert alloc] init];
    [pausePanel addButtonWithTitle:NSLocalizedString(@"Continue",@"Continue printing")];
    pausePanel.messageText = NSLocalizedString(@"Paused",@"Paused");
    pausePanel.informativeText = NSLocalizedString(@"You can also add a pause command into the G-Code. Just add a line like\n@pause Change filament\ninto your code.",
                                                   @"You can also add a pause command into the G-Code. Just add a line like\n@pause Change filament\ninto your code.");
    preferences = [Preferences new];
    emergencyButton.tag = 0;
    pausePanel.alertStyle = NSInformationalAlertStyle;
    slicerConf = [[SlicerConfig alloc] init];
    stlHistory = [[RHFileHistory alloc] initWithName:@"stlHistory" max:20];
    [stlHistory attachMenu:openRecentSTLMenu withSelector:@selector(openRecentSTL:)];
    gcodeHistory = [[RHFileHistory alloc] initWithName:@"gcodeHistory" max:20];
    [gcodeHistory attachMenu:openRecentGCodeMenu withSelector:@selector(openRecentGCode:)];
    [PrinterConfiguration fillFormsWithCurrent];
    //[logScrollView setScrollerStyle:NSScrollerStyleLegacy];
    [logSplitterDelegate setAutosaveName:@"logSplitterHeight"];
    [editorSplitterDelegate setAutosaveName:@"editorSplitterWidth"];
    [self updateViewTravel];
    [RHSound createSounds];

  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
  NSString *exe = [d stringForKey:@"slicerPath"];
  BOOL fileExists;
  BOOL isDir;
  fileExists = [fileManager fileExistsAtPath:exe isDirectory:&isDir];
  fileExists&=!isDir;
  if(!fileExists) {
    // set the external path if it is not set already,
    // there is no internal app anymore.
    [preferences browseSlicerExecuteable:nil];
  }

  NSString * appBundleID = [[NSBundle mainBundle] bundleIdentifier];
  NSArray  * paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
  NSString * supportPath = [paths[0] stringByAppendingPathComponent:appBundleID];
  fileExists = [fileManager fileExistsAtPath:supportPath isDirectory:&isDir];
  fileExists &= !isDir;
  if ( fileExists ) { // remove if not a directory
    NSError *err= nil;
    [fileManager removeItemAtPath:supportPath error:&err];
  }

  NSError *err;
  if ([fileManager fileExistsAtPath: supportPath] == NO)
    {
    err= nil;
    [fileManager createDirectoryAtPath: supportPath withIntermediateDirectories:YES attributes:nil error:&err];
    }
  [d setObject:supportPath forKey:@"appSupportFolderPath"];

  NSString * tempFolder = NSTemporaryDirectory();
  NSString * tempPath = [tempFolder stringByAppendingPathComponent:appBundleID];
  fileExists = [fileManager fileExistsAtPath:tempPath isDirectory:&isDir];
  if (!fileExists)
    {
    err= nil;
    [fileManager createDirectoryAtPath: tempPath
           withIntermediateDirectories:YES
                            attributes:nil error:&err];
    }
  [d setObject:tempPath forKey:@"appTempFolderPath"];

  [creditsText readRTFDFromFile:[[NSBundle mainBundle] pathForResource:@"Credits" ofType:@"rtf"]];
  [firstStepsText readRTFDFromFile:[[NSBundle mainBundle] pathForResource:@"introduction" ofType:@"rtf"]];
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"showFirstSteps"]) {
     [firstStepsWindow orderFrontRegardless];
     [firstStepsWindow setLevel:NSFloatingWindowLevel];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"showFirstSteps"];
  }

}

#pragma mark -
#pragma mark Helper Functions

- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename {
    NSString *ext = filename.pathExtension;
    if([ext compare:@"gcode" options:NSCaseInsensitiveSearch]==NSOrderedSame) {
        [gcodeView loadGCodeGCode:filename];
        return YES;
    } else if([ext compare:@"stl" options:NSCaseInsensitiveSearch]==NSOrderedSame) {
        [stlComposer loadSTLFile:filename];
        return YES;
    }
    return NO;
}
-(void)clearGraphicContext {
    [codePreview clearGraphicContext];
    [stlView clearGraphicContext];
    [printPreview clearGraphicContext];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    if(connection->job->mode==1) {
        [self showWarning:@"Stop your printing process before quitting the program!" headline:@"Termination aborted"];
        return NO; 
    }
    if(!connection.close) return NO;
    return YES;
}
- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag {
    
    [self.window makeKeyAndOrderFront:self];
    
    return YES;
}
- (void)windowDidBecomeKey:(NSNotification *)notification {
    [stlComposer recheckChangedFiles];
}
- (IBAction)toolConnect:(id)sender {
  if(connection->connected){
        [connection close];
  } else {
        [connection open];
  }
}

- (IBAction)openPrinterSettings:(id)sender {
    if(!printerSettingsController) {
        printerSettingsController = [[PrinterSettingsController alloc] init];
  //    initWithWindow:self.window];
  //   [printerSettingsController showWindow:self];
    }
  //  [printerSettingsController showWindow:self];
    [printerSettingsController.window makeKeyAndOrderFront:nil];
}
-(void)updateJobButtons {
    if(connection->connected) {
        [runJobButton setTag:YES];
        killJobButton.tag = connection->job.hasData;
        pauseJobButton.tag = connection->job.hasData;
        [sdcardButton setTag:YES];
    } else {
        [runJobButton setTag:NO];
        [killJobButton setTag:NO];
        [pauseJobButton setTag:NO];
        [sdcardButton setTag:NO];
    }
  [toolbar validateVisibleItems];
}


-(void)updateViewFilament {
    if(conf3d->disableFilamentVisualization) {
        showFilamentButton.label = @"Hides filament";
        showFilamentButton.image = hideFilamentIcon;
    } else {
        showFilamentButton.label = @"Shows filament";
        showFilamentButton.image = viewFilamentIcon;
    }
    [openGLView redraw];
}
-(void)updateViewTravel {
    if(!conf3d->showTravel) {
        showTravelButton.label = @"Hides travel";
        showTravelButton.image = hideFilamentIcon;
    } else {
        showTravelButton.label = @"Shows travel";
        showTravelButton.image = viewFilamentIcon;
    }
}

#pragma mark -
#pragma mark Notifications

/** Replaces the current preview model with the one
 computed in a different thread. 
 */
-(void)replaceGCodeView:(NSNotification*)event {
    [codePreview->models remove:codeVisual];
    codeVisual = event.object;
    [codePreview->models addLast:codeVisual];
    [openGLView redraw];
    @synchronized(gcodeView->editor->timer) {
        gcodeView->editor->nextView = nil;
    }
}

-(void)progressReceived:(NSNotification*)notification {
    NSNumber *n = notification.object;
    printProgress.doubleValue = n.doubleValue;
}
-(void)printerInfoReceived:(NSNotification*)notification {
    printStatusLabel.stringValue = notification.object;
}
-(void)temperatureRead:(NSNotification*)notification {
    NSMutableString *tr = [NSMutableString stringWithCapacity:50];
    [connection->tempLock lock];
    if(connection->extruderTemp.count>1) {
        for(NSNumber *key in connection->extruderTemp) {
            int e = key.intValue;
            [tr appendFormat:NSLocalizedString(@"Extruder %d: %1.2f°C",@"Extruder %d: %1.2f°C"),e+1,[connection getExtruderTemperature:e]];
            if ([connection->analyzer getExtruderTemperature:e]>0)
                [tr appendFormat:NSLocalizedString(@"/%1.0f°C",@"/%1.0f°C"),[connection->analyzer getExtruderTemperature:e]];
            else
                [tr appendString:@"/Off "];
        }
    } else if(connection->extruderTemp.count==1){
        [tr appendFormat:NSLocalizedString(@"Extruder: %1.2f°C",@"Extruder: %1.2f°C"),[connection getExtruderTemperature:-1]];
        if ([connection->analyzer getExtruderTemperature:-1]>0)
            [tr appendFormat:NSLocalizedString(@"/%1.0f°C",@"/%1.0f°C"),[connection->analyzer getExtruderTemperature:-1]];
        else
            [tr appendString:NSLocalizedString(@"/Off",@"/Off")];
    }
    [connection->tempLock unlock];
    if (connection->bedTemp > 0)
    {
        [tr appendFormat:NSLocalizedString(@"Bed: %1.2f",@"Bed: %1.2f"),connection->bedTemp];
        if (connection->analyzer->bedTemp > 0)
            [tr appendFormat:NSLocalizedString(@"/%1.0f°C",@"/%1.0f°C"),connection->analyzer->bedTemp];
        else
            [tr appendString:NSLocalizedString(@"°C/Off",@"°C/Off"])];
    }
    printTempLabel.stringValue = tr;
}

-(void)jobChanged:(NSNotification*)notification {
    [self updateJobButtons];
}
- (void)connectionOpened:(NSNotification *)notification {
    connectButton.label = NSLocalizedString(@"Disconnect",@"Disconnect");
    connectButton.image = disconnectImage;
  //[manualControl updateConnectionStatus:YES];

    printTempLabel.stringValue = NSLocalizedString(@"Waiting for temperature",
                                                   @"Waiting for temperature");
    [eepromMenuItem setEnabled:YES];
    [emergencyButton setTag:YES];
    [sendScript1Menu setEnabled:YES];
    [sendScript2Menu setEnabled:YES];
    [sendScript3Menu setEnabled:YES];
    [sendScript4Menu setEnabled:YES];
    [sendScript5Menu setEnabled:YES];
}

-(void)firmwareDetected:(NSNotification*)event {
    firmwareLabel.stringValue = event.object;
}

- (void)connectionClosed:(NSNotification *)notification {
    connectButton.label = @"Connect";
    connectButton.image = connectImage;
    printTempLabel.stringValue = @"Disconnected";    
    [eepromMenuItem setEnabled:NO];
    [emergencyButton setTag:NO];
    [sendScript1Menu setEnabled:NO];
    [sendScript2Menu setEnabled:NO];
    [sendScript3Menu setEnabled:NO];
    [sendScript4Menu setEnabled:NO];
    [sendScript5Menu setEnabled:NO];
    [self updateJobButtons];
}

#pragma mark -
#pragma mark UI Actions

-(void)openRecentSTL:(NSMenuItem*)item {
    NSString *file = item.title;
    [composer loadSTLFile:file];
}
-(void)openRecentGCode:(NSMenuItem*)item {
    NSString *file = item.title;
    [gcodeView loadGCodeGCode:file];
}
- (IBAction)toggleLog:(id)sender {
    //  [topLogView setHidden:![topLogView isHidden]];
}

- (IBAction)toggleETAAction:(id)sender {
    connection->job->etaTimeLeft = !connection->job->etaTimeLeft;
}

- (IBAction)showEEPROM:(NSMenuItem *)sender {
    if(!eepromController) {
        eepromController = [[EEPROMController alloc] init];
    }
    [eepromController update];
    [eepromController.window makeKeyAndOrderFront:nil];
}

- (IBAction)runJobAction:(id)sender {
  RHPrintjob *job = connection->job;
//  if (job->dataComplete)
//    {
//    [connection pause:@"You can also add a pause command into the G-Code. Just add a line like\n@pause Change filament\ninto your code."];
//
//    //  conn.pause("Press OK to continue.\n\nYou can add pauses in your code with\n@pause Some text like this");
//    }
//  else
    if (!job->dataComplete) {
    //   runJobButton.label = @"Pause";
    //    runJobButton.image = pauseJobIcon;
    openGLView->act = app->printPreview;

    // toolRunJob.Image = imageList.Images[3];
    [printVisual clear];
    [job beginJob];
    [job pushShortArray:gcodeView->prepend->textArray];
    [job pushShortArray:gcodeView->gcode->textArray];
    [job pushShortArray:gcodeView->append->textArray];
    [job endJob ];
   }
}


- (IBAction)pauseJobAction:(id)sender {
  RHPrintjob *job = connection->job;
  if (job->dataComplete)
    {
    [connection pause:@"You can also add a pause command into the G-Code. Just add a line like\n@pause Change filament\ninto your code."];
    //  conn.pause("Press OK to continue.\n\nYou can add pauses in your code with\n@pause Some text like this");
    }
}


- (IBAction)killJobAction:(id)sender {
    runJobButton.label = @"Run";
    runJobButton.image = runJobIcon;
    [connection->job killJob];
    [self updateJobButtons];
}

- (IBAction)saveGCodeAction:(id)sender {
    [gcodeView saveGCode:sender];
}

- (IBAction)loadGCodeAction:(id)sender {
  [openPanel beginSheetModalForWindow:mainWindow completionHandler:^(NSInteger result){
    if (result == NSFileHandlingPanelOKButton) {
      NSArray* urls = openPanel.URLs;
      if(urls.count>0) {
        [gcodeView loadGCodeGCode:[urls[0] path]];
      }
      // Use the URLs to build a list of items to import.
    }
  }];
}

- (IBAction)sdCardAction:(id)sender {
    if(!sdcardManager) {
        sdcardManager = [[SDCardManager alloc] init];
    }
    [sdcardManager.window makeKeyAndOrderFront:nil];
}

- (IBAction)showFilamentAction:(NSToolbarItem *)sender {
    conf3d->disableFilamentVisualization = !conf3d->disableFilamentVisualization;
    [self updateViewFilament];
}

- (IBAction)showTravelAction:(id)sender {
    NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
    [d setObject:@(!conf3d->showTravel) forKey:@"threedShowTravel"];
}

- (IBAction)showPreferences:(NSMenuItem *)sender {
    [preferences->prefWindow makeKeyAndOrderFront:nil];
}

- (IBAction)showSlicerPanel:(NSMenuItem *)sender {
    [slicerPanel makeKeyAndOrderFront:nil];
}

- (IBAction)emergencyAction:(id)sender {
    if (!connection->connected) return;
    connection->closeAfterM112 = NO; //YES;
    [connection injectManualCommandFirst:@"M112"];
    [connection->job killJob];
    [rhlog addError:NSLocalizedString(@"Send emergency stop to printer. You may need to reset the printer for a restart!", @"Send emergency stop to printer. You may need to reset the printer for a restart!")];
    // Try to reset board by toggling DTR line
    [connection sendReset];
   /* while ([connection hasInjectedMCommand:112])
    {
        [NSApplication 
        Application.DoEvents();
    }
    [connection close];*/
}
- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode
        contextInfo:(void *)contextInfo {
    
}
-(void)showWarning:(NSString*)warn headline:(NSString*)head {
    NSAlert *alert = [[[NSAlert alloc] init] autorelease];
    [alert addButtonWithTitle:@"OK"];
    alert.messageText = head;
    alert.informativeText = warn;
    alert.alertStyle = NSWarningAlertStyle;
    [alert beginSheetModalForWindow:mainWindow modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}
-(void)showHeaterRunning {
    [heaterWarningPanel center];
    [heaterWarningPanel makeKeyAndOrderFront:nil];
    // [heaterWarningPanel orderFrontRegardless];
}
- (IBAction)pausedPanelContinue:(id)sender {
    [pausedPanel orderOut:window];
    [connection pauseDidEnd];
}

- (IBAction)showHomepage:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://rssdev.net"]];
}

- (IBAction)showSlic3rHomepage:(id)sender {
  [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://slic3r.org"]];
}

- (IBAction)showPrusaSlicerHomepage:(id)sender {
  [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://www.prusa3d.com"]];
}

- (IBAction)showWorkdir:(id)sender {
  NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
  NSString * folder = [d stringForKey:@"appTempFolderPath"];
  [[NSWorkspace sharedWorkspace] openFile:folder ];
}

- (IBAction)sendScript1Action:(id)sender {
    for (GCodeShort *code in gcodeView->script1->textArray)
    {
        [connection injectManualCommand:code->text];
    }
}

- (IBAction)sendScript2Action:(id)sender {
    for (GCodeShort *code in gcodeView->script2->textArray)
    {
        [connection injectManualCommand:code->text];
    }
}

- (IBAction)sendScript3Action:(id)sender {
    for (GCodeShort *code in gcodeView->script3->textArray)
    {
        [connection injectManualCommand:code->text];
    }
}

- (IBAction)sendScript4Action:(id)sender {
    for (GCodeShort *code in gcodeView->script4->textArray)
    {
        [connection injectManualCommand:code->text];
    }
}

- (IBAction)sendScript5Action:(id)sender {
    for (GCodeShort *code in gcodeView->script5->textArray)
    {
        [connection injectManualCommand:code->text];
    }
}

- (IBAction)donateAction:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.repetier.com/donate-or-support/"]];}

- (IBAction)heaterRunningAbort:(id)sender {
    [heaterWarningPanel orderOut:window];
}

- (IBAction)heaterRunningDisconnect:(id)sender {
    [heaterWarningPanel orderOut:window];
    [connection forceClose];
}
@end

RHAppDelegate *app;
