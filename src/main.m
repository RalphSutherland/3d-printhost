/*
 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import <Cocoa/Cocoa.h>
#import "PrinterConfiguration.h"
#import "PrinterConnection.h"
#import "IntegerTransformer.h"
#import "ThreeDConfig.h"
#import "RHLogger.h"
#import "DefaultsExtension.h"
#import "models/Slicer.h"

int main(int argc, char *argv[])
{
    // Set default values
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    d[@"antialiasMethod"] = @1;
    d[@"antialiasSamples"] = @4;

   NSString * tempPath = NSTemporaryDirectory();
//  NSString *folder = paths[0];
//  NSString* appBundleID = [[NSBundle mainBundle] bundleIdentifier];
//  d[@"appSupportFolder"]   = [folder stringByAppendingPathComponent:appBundleID];
  
  d[@"appSupportFolder"]   = tempPath;
  d[@"appSupportConfFile"] = @"slicer.ini";
  d[@"appSupportSTLFile"]  = @"tempobj.stl";
  d[@"appSupportLogFile"]  = @"slicer.log";

    d[@"threedFilamentVisualization"] = @1;
    d[@"threedFacesColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:1.000 green:0.000 blue:0.000 alpha:1.000]];
    d[@"threedSelectedFacesColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.833 saturation:1.000 brightness:1.000 alpha:1.000]];
    d[@"threedEdgesColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.833 saturation:1.000 brightness:0.500 alpha:1.000]];
    d[@"threedSelectedEdgesColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.167 saturation:1.000 brightness:1.000 alpha:1.000]];

    d[@"threedOutsidePrintbedColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.443 saturation:0.500 brightness:1.000 alpha:1.000]];
    d[@"threedSelectionBoxColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithCalibratedRed:0.117 green:0.564 blue:1 alpha:1]];

    d[@"threedFilamentColor"] =  (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.556 saturation:0.796 brightness:0.804 alpha:1.000]];
    d[@"threedFilamentColor2"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.708 saturation:1.000 brightness:1.000 alpha:1.000]];
    d[@"threedFilamentColor3"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.304 saturation:1.000 brightness:1.000 alpha:1.000]];
    d[@"threedHotFilamentColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:1.000 green:0.000 blue:0.000 alpha:1.000]];
    d[@"threedSelectedFilamentColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.500 saturation:1.000 brightness:0.600 alpha:1.000]];
    
    d[@"threedBackgroundColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.548 saturation:0.084 brightness:0.929 alpha:1.000]];
    d[@"threedPrinterColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.167 saturation:0.080 brightness:1.000 alpha:1.000]];
    d[@"threedPrinterRotCenterColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.167 saturation:1.000 brightness:0.300 alpha:1.000]];
    d[@"threedPrinterBottomColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.019 saturation:0.218 brightness:1.000 alpha:1.000]];

    d[@"threedLight1AmbientColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor blackColor]];
    d[@"threedLight2AmbientColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor blackColor]];
    d[@"threedLight3AmbientColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor blackColor]];
    d[@"threedLight4AmbientColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor blackColor]];
    d[@"threedLight1DiffuseColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight2DiffuseColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight3DiffuseColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight4DiffuseColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight1SpecularColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight2SpecularColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight3SpecularColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedLight4SpecularColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1]];
    d[@"threedBottomTransparency"] = @20.0;
    d[@"threedHotFilamentLength"] = @1000.0;
    d[@"threedLight1XDir"] = @-1.0;
    d[@"threedLight1YDir"] = @-2.0;
    d[@"threedLight1ZDir"] = @2.0;
    d[@"threedLight2XDir"] = @1.0;
    d[@"threedLight2YDir"] = @2.0;
    d[@"threedLight2ZDir"] = @3.0;
    d[@"threedLight3XDir"] = @1.0;
    d[@"threedLight3YDir"] = @-2.0;
    d[@"threedLight3ZDir"] = @2.0;
    d[@"threedLight4XDir"] = @1.7;
    d[@"threedLight4YDir"] = @-1.0;
    d[@"threedLight4ZDir"] = @-2.5;
    d[@"threedLight1Enabled"] = @1;
    d[@"threedLight2Enabled"] = @1;
    d[@"threedLight3Enabled"] = @0;
    d[@"threedLight4Enabled"] = @0;
    d[@"threedShowPrintbed"] = @1;
    d[@"threedShowTravel"] = @1;
    d[@"threedAccelerationMethod"] = @0;
    d[@"threedDrawEdges"] = @0;
    d[@"threedHeightMethod"] = @0;
    d[@"threedLayerHeight"] = @0.38;
    d[@"threedWidthOverHeight"] = @1.6;
    d[@"threedFilamentDiameter"] = @2.87;
    d[@"disableFilamentVisualization"] = @0;
    d[@"threedShowPerspective"] = @1;
    d[@"correctNormals"] = @1;


    //Slicer defaults
    d[@"slicerExternalPath"] = @"";
    d[@"slicerExternalConfig"] = @"";

    // Editor colors
    d[@"editorCommandColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.000 green:0.000 blue:0.636 alpha:1.000]];
    d[@"editorParameterIndicatorColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.730 green:0.000 blue:0.012 alpha:1.000]];
    d[@"editorParameterValueColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:0.000 alpha:1.000]];
    d[@"editorCommentColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:0.600 alpha:1.000]];
    d[@"editorHostCommandColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.262 green:0.787 blue:0.000 alpha:1.000]];

    d[@"editorSelectedTextColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:1.000 alpha:1.000]];
    d[@"editorSelectedBackgroundColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.140 green:0.513 blue:0.700 alpha:1.000]];

    d[@"editorLineTextColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:1.000 alpha:1.000]];
    d[@"editorLineBackgroundColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.480 green:0.530 blue:0.600 alpha:1.000]];
    d[@"editorLineBackgroundAltColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.680 green:0.750 blue:0.850 alpha:1.000]];

    d[@"editorBackgroundOddColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:1.000 alpha:1.000]];
    d[@"editorBackgroundEvenColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.900 green:0.937 blue:0.980 alpha:1.000]];

    // Temp monitor colors
    d[@"tempBackgroundColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:0.400 alpha:1.000]];
    d[@"tempGridColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:1.000 green:1.000 blue:0.799 alpha:0.755]];
    d[@"tempAxisColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:1.000 alpha:1.000]];
    d[@"tempFontColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:1.000 alpha:1.000]];

    d[@"tempExtruderColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:1.000 green:0.000 blue:0.000 alpha:1.000]];
    d[@"tempAvgExtruderColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.500 green:0.000 blue:0.000 alpha:0.750]];
    d[@"tempTargetExtruderColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.750 green:0.500 blue:1.000 alpha:1.000]];


    d[@"tempBedColor"]       = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.333 green:1.000 blue:0.000 alpha:1.000]];
    d[@"tempAvgBedColor"]    = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.167 green:0.501 blue:0.000 alpha:0.750]];
    d[@"tempTargetBedColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.500 green:1.000 blue:0.996 alpha:1.000]];

    d[@"tempExtPowerColor"]    = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.512 saturation:1.000 brightness:1.000 alpha:0.500]];
    d[@"tempExtAvgPowerColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.628 saturation:1.000 brightness:1.000 alpha:0.750]];

    d[@"tempBedPowerColor"]    = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.822 saturation:1.000 brightness:1.000 alpha:0.500]];
    d[@"tempBedAvgPowerColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.987 saturation:1.000 brightness:1.000 alpha:0.750]];

    d[@"tempTotPowerColor"]    = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.178 saturation:1.000 brightness:1.000 alpha:0.500]];
    d[@"tempTotAvgPowerColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceHue:0.153 saturation:1.000 brightness:1.000 alpha:1.000]];

    d[@"threedTravelColor"]  = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.79 green:1.0 blue:0.9 alpha:1]];
    d[@"tempAverageSeconds"] = @3;
    d[@"tempZoomLevel"]      = @3;
    d[@"tempShowExtruder"]   = @YES;
    d[@"tempShowAverage"]    = @YES;
    d[@"tempShowBed"]        = @YES;
    d[@"tempAutoscroll"]     = @YES;
    d[@"tempShowOutput"]     = @NO;
    d[@"tempShowTarget"]     = @YES;
    d[@"tempExtruderWidth"]  = @2.0;
    d[@"tempAvgExtruderWidth"] = @2.0;
    d[@"tempTargetExtruderWidth"] = @1.0;
    d[@"tempBedWidth"] = @2.0;
    d[@"tempAvgBedWidth"] = @2.0;
    d[@"tempTargetBedWidth"] = @1.0;
    d[@"tempAvgOutputWidth"] = @2.0;
    // Logs
    d[@"log.sendEnabled"] = @NO;
    d[@"log.infoEnabled"] = @YES;
    d[@"log.warningsEnabled"] = @YES;
    d[@"log.errorsEnabled"] = @YES;
    d[@"logs.ackEnabled"] = @NO;
    d[@"logs.autoscrollEnabled"] = @YES;
    d[@"logsWriteToFile"] = @YES;

    d[@"logDefaultColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:0.000 alpha:1.000]];
    d[@"logInformationColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.000 green:0.150 blue:0.300 alpha:1.000]];
    d[@"logWarningColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:1.000 green:0.417 blue:0.000 alpha:1.000]];
    d[@"logErrorColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:1.000 green:0.000 blue:0.000 alpha:1.000]];

    d[@"logSelectedTextColor"]   = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor whiteColor]];
    d[@"logSelectedBackgroundColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.453 green:0.587 blue:0.777 alpha:1.000]];
    d[@"logLineTextColor"]       = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor whiteColor]];
    d[@"logLineBackgroundColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.498 green:0.551 blue:0.627 alpha:1.000]];
    d[@"logBackgroundOddColor"]  = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor whiteColor]];
    d[@"logBackgroundEvenColor"] = (NSData*)[NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.900 green:0.937 blue:0.980 alpha:1.000]];

    // Slicer default
    d[@"slicerPath"] = @"";
    d[@"slicerCurrent"] = @"Default";
    d[@"slicerConfigs"] = @"Default"; // Tab seperated list
//    d[@"slicer#Default#nozzleDiameter"] = @0.4;
//    d[@"slicer#Default#useRealtiveE"] = @NO;
//    d[@"slicer#Default#zOffset"] = @0.0;
//    d[@"slicer#Default#filamentDiameter"] = @1.75;
//    d[@"slicer#Default#extrusionMultiplier"] = @1.0;
//    d[@"slicer#Default#temperature"] = @200;
//    d[@"slicer#Default#perimeterSpeed"] = @30.0;
//    d[@"slicer#Default#smallPerimeterSpeed"] = @30.0;
//    d[@"slicer#Default#infillSpeed"] = @60.0;
//    d[@"slicer#Default#solidInfillSpeed"] = @60.0;
//    d[@"slicer#Default#bridgesSpeed"] = @60.0;
//    d[@"slicer#Default#travelSpeed"] = @130.0;
//    d[@"slicer#Default#firstLayerSpeed"] = @20.0;
//    d[@"slicer#Default#layerHeight"] = @0.4;
//    d[@"slicer#Default#firstLayerHeight"] = @0.4;
//    d[@"slicer#Default#infillEveryNLayers"] = @1.0;
//    d[@"slicer#Default#skirtLoops"] = @1;
//    d[@"slicer#Default#skirtDistance"] = @6.0;
//    d[@"slicer#Default#skirtHeight"] = @1;
//    d[@"slicer#Default#perimeters"] = @3;
//    d[@"slicer#Default#solidLayers"] = @3;
//    d[@"slicer#Default#fillDensity"] = @0.4;
//    d[@"slicer#Default#fillAngle"] = @45.0;
//    d[@"slicer#Default#retractLength"] = @3.0;
//    d[@"slicer#Default#retractZLift"] = @0.0;
//    d[@"slicer#Default#retractSpeed"] = @30.0;
//    d[@"slicer#Default#retractExtraLength"] = @0.0;
//    d[@"slicer#Default#retractMinTravel"] = @2.0;
//    d[@"slicer#Default#extrusionWidth"] = @0.0;
//    d[@"slicer#Default#bridgeFlowRatio"] = @1.0;
//    d[@"slicer#Default#fillPattern"] = @"rectilinear";
//    d[@"slicer#Default#solidFillPattern"] = @"rectilinear";
//    d[@"slicer#Default#comments"] = @NO;
//    d[@"slicer#Default#coolBridgeFanSpeed"] = @100;
//    d[@"slicer#Default#coolDisplayLayer"] = @1;
//    d[@"slicer#Default#coolEnableBelow"] = @60;
//    d[@"slicer#Default#coolMaxFanSpeed"] = @100;
//    d[@"slicer#Default#coolMinFanSpeed"] = @35;
//    d[@"slicer#Default#coolMinPrintSpeed"] = @10;
//    d[@"slicer#Default#coolSlowDownBelow"] = @15;
//    d[@"slicer#Default#GCodeFlavor"] = @"RepRap (Repetier/Marlin/Sprinter)";
//    d[@"slicer#Default#supportMaterialTool"] = @"Primary";
//    d[@"slicer#Default#firstLayerTemperature"] = @200;
//    d[@"slicer#Default#coolEnable"] = @NO;
//    d[@"slicer#Default#generateSupportMaterial"] = @NO;
//    d[@"slicer#Default#keepFanAlwaysOn"] = @NO;
//    d[@"slicer#Default#bedtemperature"] = @0;
//    d[@"slicer#Default#firstLayerBedTemperature"] = @0;
//    d[@"slicer#Default#randomizeStartingPoint"] = @YES;
//    d[@"slicer#Default#threads"] = @4;
    d[@"slicerVersionGroup"] = @0;
    
    // Other data
    d[@"extruder.Speed"] = @60.0;
    d[@"extruder.extrudeLength"] = @10.0;
    d[@"extruder.retract"] = @3.0;
    d[@"showFirstSteps"] = @YES;
    d[@"logSplitterHeight"] = @170.0f;
    d[@"editorSplitterWidth"] = @516.0f;
    // Sounds
    d[@"soundPrintjobFinished"] = @"";
    d[@"soundPrintjobFinishedEnabled"] = @NO;
    d[@"soundPrintjobPaused"] = @"";
    d[@"soundPrintjobPausedEnabled"] = @NO;
    d[@"soundError"] = @"";
    d[@"soundErrorEnabled"] = @NO;
    d[@"soundCommand"] = @"";
    d[@"soundCommandEnabled"] = @NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults registerDefaults:d];

    connection = nil;
    [PrinterConfiguration initPrinter];
    conf3d = [ThreeDConfig new];
    //rhlog = [[RHLogger alloc] init];
    connection = [[PrinterConnection alloc] init];
    [pool release];
    return NSApplicationMain(argc, (const char **)argv);
}
