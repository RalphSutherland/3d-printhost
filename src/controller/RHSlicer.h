/*

 Modified 2017 RSS

 Copyright 2011 repetier repetierdev@gmail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import <Cocoa/Cocoa.h>

@interface RHSlicer : NSView {
    NSView *view;
    NSButton *slicerActive;
    NSPopUpButton *slicerPrinterSettings;
    NSPopUpButton *slicerPrintSettings;
    NSPopUpButton *slicerFilamentSettings;
    NSPopUpButton *slicerFilamentSettings2;
    NSPopUpButton *slicerFilamentSettings3;
    NSMutableArray *slicerFilamentList;
    NSMutableArray *slicerPrintList;
    NSMutableArray *slicerPrinterList;
    NSButton *runSlice;
    NSButtonCell *killButton;
}

@property (assign) IBOutlet NSView *view;
@property (assign) IBOutlet NSButton *slicerActive;
@property (assign) IBOutlet NSPopUpButton *slicerPrinterSettings;
@property (assign) IBOutlet NSPopUpButton *slicerPrintSettings;
@property (assign) IBOutlet NSPopUpButton *slicerFilamentSettings;
@property (assign) IBOutlet NSPopUpButton *slicerFilamentSettings2;
@property (assign) IBOutlet NSPopUpButton *slicerFilamentSettings3;
@property (retain) NSMutableArray *slicerFilamentList;
@property (retain) NSMutableArray *slicerPrintList;
@property (retain) NSMutableArray *slicerPrinterList;
@property (assign) IBOutlet NSButton *runSlice;
@property (assign) IBOutlet NSButtonCell *killButton;

- (IBAction)sliceAction:(id)sender;
- (IBAction)configureSlicerAction:(id)sender;
//- (IBAction)selectSlicerAction:(id)sender;

- (IBAction)killAction:(id)sender;
- (void)updateSelections;

- (void)slicerConfigToVariables;
- (void)printerChanged:(NSNotification*)event;
- (void)updateBindings;

+ (NSString*)slicerConfigDir;

@end
