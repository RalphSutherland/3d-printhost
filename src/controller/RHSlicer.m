/*
 Copyright 2011 repetier repetierdev@gmail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import "RHSlicer.h"
#import "../RHAppDelegate.h"
#import "STLComposer.h"
#import "../models/PrinterConnection.h"
#import "../extensions/utils/IniFile.h"
#import "../extensions/utils/StringUtil.h"
#import "GCodeEditorController.h"

@implementation RHSlicer
@synthesize killButton;
@synthesize view;
@synthesize slicerActive;
@synthesize slicerPrinterSettings;
@synthesize slicerPrintSettings;
@synthesize slicerFilamentSettings;
@synthesize slicerFilamentSettings2;
@synthesize slicerFilamentSettings3;
@synthesize slicerFilamentList;
@synthesize slicerPrinterList;
@synthesize slicerPrintList;
@synthesize runSlice;


- (void)awakeFromNib{

    self.slicerPrintList = [[NSMutableArray new] autorelease];
    self.slicerPrinterList = [[NSMutableArray new] autorelease];
    self.slicerFilamentList = [[NSMutableArray new] autorelease];
    [self updateSelections];
    [self slicerConfigToVariables];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(printerChanged:) name:@"RHPrinterChanged" object:nil];
    [self updateBindings];

}


- (instancetype)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray * objs;
        if ([[NSBundle mainBundle ] loadNibNamed:@"Slicer"
                                           owner:self topLevelObjects:&objs])
        {
            view.frame = self.bounds;
            [self addSubview:view];
            self.slicerPrintList    = [[NSMutableArray new] autorelease];
            self.slicerPrinterList  = [[NSMutableArray new] autorelease];
            self.slicerFilamentList = [[NSMutableArray new] autorelease];
            [self updateSelections];
            [self slicerConfigToVariables];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(printerChanged:) name:@"RHPrinterChanged" object:nil];
            [self updateBindings];
        }
    }
    
    return self;
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}
-(void)slicerConfigToVariables {
    if(app!=nil)
        [app->slicer checkConfig];
    [connection.variables removeAllObjects];
    // Slic3r
        NSString *sFilament = currentPrinterConfiguration.slicerFilament1;
        NSString *sPrint    = currentPrinterConfiguration.slicerPrint;
        NSString *sPrinter  = currentPrinterConfiguration.slicerPrinter;
        NSString *cdir = [RHSlicer slicerConfigDir];
        NSString *fPrinter = [NSString stringWithFormat:@"%@/print/%@.ini",cdir,sPrint];
        IniFile *ini = [[[IniFile alloc] init] autorelease];
        [ini read:fPrinter];
        IniFile *ini2 = [[[IniFile alloc] init] autorelease];
        [ini2 read:[NSString stringWithFormat:@"%@/printer/%@.ini",cdir,sPrinter]];
        IniFile *ini3 = [[[IniFile alloc] init] autorelease];
        [ini3 read:[NSString stringWithFormat:@"%@/filament/%@.ini",cdir,sFilament]];
        [ini flatten];
        [ini2 flatten];
        [ini3 flatten];
        [connection importVariablesFormDictionary:[(ini.sections)[@""] entries]];
        [connection importVariablesFormDictionary:[(ini2.sections)[@""] entries]];
        [connection importVariablesFormDictionary:[(ini3.sections)[@""] entries]];

    if(app && app->gcodeView)
        [app->gcodeView->variablesTable.tableView reloadData];
}
-(void)setPopup:(NSPopUpButton*)pop enabled:(BOOL)enable {
    [pop removeAllItems];
    if(enable) {
        [pop addItemsWithTitles:slicerFilamentList];
        for(NSMenuItem *item in pop.itemArray) {
            item.enabled = enable;
        }
    } else {
      [pop addItemWithTitle:@"none"];
      for(NSMenuItem *item in pop.itemArray) {
        item.enabled = enable;
      }
    }
    pop.enabled = enable;
    pop.cell.enabled = enable;
}

-(void) updateSelections {
    NSString *cdir = [RHSlicer slicerConfigDir];
    NSString *oldFilament = currentPrinterConfiguration.slicerFilament1;
    NSString *oldFilament2 = currentPrinterConfiguration.slicerFilament2;
    NSString *oldFilament3 = currentPrinterConfiguration.slicerFilament3;
    NSString *oldPrint = currentPrinterConfiguration.slicerPrint;
    NSString *oldPrinter = currentPrinterConfiguration.slicerPrinter;
    // Filament list
    NSArray* enumerator = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/filament",cdir] error:nil];

    [slicerFilamentList removeAllObjects];
    for (NSString *file in enumerator)
    {
        // check if it's a directory
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/filament/%@",cdir,file] isDirectory: &isDirectory];
        if (!isDirectory && [file.pathExtension compare:@"ini"]==NSOrderedSame)
        {
            [slicerFilamentList addObject:file.stringByDeletingPathExtension];
        }
    }

    [self setPopup:slicerFilamentSettings enabled:currentPrinterConfiguration->numberOfExtruder>0];
    [self setPopup:slicerFilamentSettings2 enabled:currentPrinterConfiguration->numberOfExtruder>1];
    [self setPopup:slicerFilamentSettings3 enabled:currentPrinterConfiguration->numberOfExtruder>2];

    // Print list
    enumerator = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/print",cdir] error:nil];
    [slicerPrintList removeAllObjects];
    for (NSString *file in enumerator)
    {
        // check if it's a directory
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/print/%@",cdir,file] isDirectory: &isDirectory];
        if (!isDirectory && [file.pathExtension compare:@"ini"]==NSOrderedSame)
        {
            [slicerPrintList addObject:file.stringByDeletingPathExtension];
        }
    }
    [slicerPrintSettings removeAllItems];
    [slicerPrintSettings addItemsWithTitles:slicerPrintList];

    // Printer list
    enumerator = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/printer",cdir] error:nil];    [slicerPrinterList removeAllObjects];
    for (NSString *file in enumerator)
    {
        // check if it's a directory
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/printer/%@",cdir,file] isDirectory: &isDirectory];
        if (!isDirectory && [file.pathExtension compare:@"ini"]==NSOrderedSame)
        {
            [slicerPrinterList addObject:file.stringByDeletingPathExtension];
        }
    }
    [slicerPrinterSettings removeAllItems];
    [slicerPrinterSettings addItemsWithTitles:slicerPrinterList];

    if(oldFilament==nil && slicerFilamentList.count>0)
        oldFilament = slicerFilamentList[0];
  if ((oldFilament) && (slicerFilamentSettings.isEnabled)) {
        currentPrinterConfiguration.slicerFilament1 = oldFilament;
        [slicerFilamentSettings selectItemWithTitle:oldFilament];
    }
    if(oldFilament2==nil && slicerFilamentList.count>0)
        oldFilament2 = slicerFilamentList[0];
    if ((oldFilament2) && (slicerFilamentSettings2.isEnabled)) {
        currentPrinterConfiguration.slicerFilament2 = oldFilament2;
        [slicerFilamentSettings2 selectItemWithTitle:oldFilament2];
    }
    if (oldFilament3==nil && slicerFilamentList.count>0)
        oldFilament3 = slicerFilamentList[0];
    if ((oldFilament3)&&(slicerFilamentSettings3.isEnabled)) {
        currentPrinterConfiguration.slicerFilament3 = oldFilament3;
        [slicerFilamentSettings3 selectItemWithTitle:oldFilament3];
    }
    if (oldPrint==nil && slicerPrintList.count>0)
        oldPrint = slicerPrintList[0];
    if ((oldPrint) &&(slicerPrintSettings.isEnabled)) {
        currentPrinterConfiguration.slicerPrint = oldPrint;
        [slicerPrintSettings selectItemWithTitle:oldPrint];
    }
    if (oldPrinter==nil && slicerPrinterList.count>0)
        oldPrinter = slicerPrinterList[0];
    if ((oldPrinter) &&(slicerPrinterSettings.isEnabled)) {
        currentPrinterConfiguration.slicerPrinter = oldPrinter;
        [slicerPrinterSettings selectItemWithTitle:oldPrinter];
    }
}
-(void)printerChanged:(NSNotification*)event {
    [self updateBindings];
    if(app!=nil)
        [app->slicer checkConfig];
}
-(void)updateBindings {
    [slicerPrintSettings bind:@"selectedValue"
                     toObject:currentPrinterConfiguration withKeyPath:@"slicerPrint" options:nil];
    [slicerPrinterSettings bind:@"selectedValue"
                       toObject:currentPrinterConfiguration withKeyPath:@"slicerPrinter" options:nil];
    [slicerFilamentSettings bind:@"selectedValue"
                        toObject:currentPrinterConfiguration withKeyPath:@"slicerFilament1" options:nil];
    [slicerFilamentSettings2 bind:@"selectedValue"
                         toObject:currentPrinterConfiguration withKeyPath:@"slicerFilament2" options:nil];
    [slicerFilamentSettings3 bind:@"selectedValue"
                         toObject:currentPrinterConfiguration withKeyPath:@"slicerFilament3" options:nil];
}
- (IBAction)sliceAction:(id)sender {
    [app->composer generateGCodeAction:sender];
}
- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}

- (IBAction)configureSlicerAction:(id)sender {
    [app->slicer configSlicerAction:sender];
}

- (IBAction)killAction:(id)sender {
    [app->slicer killSlicing];
}

+(NSString*) slicerConfigDir {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
  NSString *userpath = paths[0];
  NSInteger version = [[NSUserDefaults standardUserDefaults] integerForKey:@"slicerVersionGroup"];
  if ( version <= 0 ){
    userpath = [userpath stringByAppendingPathComponent:@"Slic3r"];
  }
  if ( version > 0 ){
    userpath = [userpath stringByAppendingPathComponent:@"PrusaSlicer"];
  }
  return userpath;
}

@end
