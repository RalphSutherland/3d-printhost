/*

 Modified 2017 RSS

 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import <Cocoa/Cocoa.h>

@interface PrefTab : NSObject {
@public
    NSString *name;
    NSTabViewItem *tab;
    NSMutableArray *children;
    BOOL showAlphaColors;
}
-(void)addTab:(NSString*)tabName tab:(id)_tab;
-(void)addTabAlpha:(NSString*)tabName tab:(id)_tab;
@property (NS_NONATOMIC_IOSONLY, readonly) int numChildren;
@end

@interface Preferences : NSWindowController<NSOutlineViewDelegate> {
    @public
    NSMutableArray *groups;
    IBOutlet NSTabViewItem *slic3rTab;
    IBOutlet NSTabViewItem *editorTab;
    IBOutlet NSTabViewItem *threedSettingsTab;
    IBOutlet NSTabViewItem *threedColorsTab;
    IBOutlet NSTabViewItem *threedLights;
    IBOutlet NSTabViewItem *temperatureColorsTab;
    IBOutlet NSTabViewItem *loggingTab;
    IBOutlet NSTabViewItem *soundsTab;
    IBOutlet NSWindow *prefWindow;
    IBOutlet NSTabView *tabView;
    IBOutlet NSOutlineView *outlineView;
    NSOpenPanel* openPanel;
    NSButton *browseSlicerConfigFile;
    NSButton *browsePrintjobFinished;
    NSButton *browsePrintjobPaused;
}
- (IBAction)openSlic3rHomepage:(id)sender;
- (IBAction)openPrusaSlicerHomepage:(id)sender;

- (IBAction)browseSlicerExecuteable:(id)sender;

- (IBAction)playPrintjobFinished:(id)sender;
- (IBAction)playPrintjobPaused:(id)sender;
- (IBAction)playError:(id)sender;
- (IBAction)playCommand:(id)sender;
- (IBAction)browsePrintjobFinished:(id)sender;
- (IBAction)browsePrintjobPaused:(id)sender;
- (IBAction)browseError:(id)sender;
- (IBAction)browseCommand:(id)sender;




@end
