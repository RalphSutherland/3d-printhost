/*
 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "Preferences.h"
#import "RHAppDelegate.h"
#import "Slicer.h"
#import "RHSound.h"

@implementation PrefTab

-(instancetype)initGroup:(NSString*)groupName {
    if((self=[super init])) {
        name = [groupName retain];
        tab = nil;
        showAlphaColors = NO;
        children = [[NSMutableArray alloc] initWithCapacity:5];        
    }
    return self;
}
-(instancetype)initTab:(NSString*)tabName tab:(id)_tab alpha:(BOOL)al {
    if((self=[super init])) {
        name = [tabName retain];
        children = nil;
        tab = _tab;
        showAlphaColors = al;
    }
    return self;
}
-(void)dealloc {
    [name release];
    if(children)
        [children release];
    [super dealloc];
}
-(void)addTab:(NSString*)tabName tab:(id)_tab {
    [children addObject:[[[PrefTab alloc] initTab:tabName tab:_tab alpha:NO] autorelease]];
}
-(void)addTabAlpha:(NSString*)tabName tab:(id)_tab {
    [children addObject:[[[PrefTab alloc] initTab:tabName tab:_tab alpha:YES] autorelease]];
}
-(int)numChildren {
    if(children==nil) return 0;
    return (int)children.count;
}
@end

@implementation Preferences

- (instancetype) init {
    if(self = [super initWithWindowNibName:@"Preferences" owner:self]) {
        groups = [[NSMutableArray alloc] initWithCapacity:10];
        [self.window setReleasedWhenClosed:NO];
    }
    return self;
}

- (instancetype)initWithWindow:(NSWindow *)_window
{
    self = [super initWithWindow:_window];
    if (self) {
        // Initialization code here.
    }
    return self;
}
-(void)dealloc {
    [groups release];
    [openPanel release];
    [super dealloc];
}

-(void) initGroups;
{
  PrefTab *g = [[PrefTab alloc] initGroup:@"Host"];
  [groups addObject:g];
  [g addTab:@"Sounds" tab:soundsTab];
  [g release];
  g = [[PrefTab alloc] initGroup:@"Slicer"];
  [groups addObject:g];
  [g addTab:@"Slic3r" tab:slic3rTab];
  [g release];
  g = [[PrefTab alloc] initGroup:@"3D visualization"];
  [groups addObject:g];
  [g addTab:@"General settings" tab:threedSettingsTab];
  [g addTab:@"Colors" tab:threedColorsTab];
  [g addTab:@"Lightning" tab:threedLights];
  [g release];
  g = [[PrefTab alloc] initGroup:@"Colors"];
  [groups addObject:g];
  [g addTab:@"Editor Colors" tab:editorTab];
  [g addTabAlpha:@"Temperature Colors" tab:temperatureColorsTab];
  [g addTab:@"Logging" tab:loggingTab];
  [g release];
  [outlineView reloadData];
}

//-(void) awakeFromNib;
//{
//    [super awakeFromNib];
//}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
  [self initGroups];
  openPanel = [[NSOpenPanel openPanel] retain];
  [openPanel setCanChooseDirectories:YES];
  [openPanel setAllowsMultipleSelection:NO];
  [[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
  [NSColor setIgnoresAlpha:NO];

}
- (BOOL)windowShouldClose:(id)sender {
    return YES;
}
- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    return (item == nil) ? groups.count : [item numChildren];
}



- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item {
    PrefTab *tab = item;
    if(tab->tab) {
        [tabView selectTabViewItem:tab->tab];
        [NSColorPanel sharedColorPanel].showsAlpha = tab->showAlphaColors;
        return YES;
    }
    return NO;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    return (item == nil) ? YES : ([item numChildren] > 0);
}


- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    
    return (item == nil) ? groups[index] : ((PrefTab *)item)->children[index];
}


- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    return (item == nil) ? @"3D-Host" : ((PrefTab*)item)->name;
}

- (IBAction)openSlic3rHomepage:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://slic3r.org/download/"]];
}

- (IBAction)openPrusaSlicerHomepage:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://www.prusa3d.com"]];
}

- (IBAction)browseSlicerExecuteable:(id)sender {

    retryEntry:

    openPanel.message = NSLocalizedString(@"Locate the Slicer Application (eg. Slic3r.app)",@"Locate the Slicer Application (eg. Slic3r.app)");

    NSInteger alertResult = -1;
    NSInteger result = [openPanel runModal];
    NSInteger version = [NSUserDefaults.standardUserDefaults integerForKey:@"slicerVersionGroup"];

    if (result == NSFileHandlingPanelOKButton) {
        NSArray* urls = openPanel.URLs;
        if(urls.count>0) {
            NSURL *url = urls[0];
            
            NSString *path = nil;
            if ( version <= 0  ) {
                path = [url.path stringByAppendingString:@"/Contents/MacOS/slic3r"];
            } else if ( version >= 1  ) {
                path = [url.path stringByAppendingString:@"/Contents/MacOS/PrusaSlicer"];
            }
            NSFileManager *fm = [NSFileManager defaultManager];
            BOOL isDir;
            BOOL fileExists = [fm fileExistsAtPath:path isDirectory:&isDir];
            fileExists&=!isDir;
            if(fileExists){
                [NSUserDefaults.standardUserDefaults setObject:path forKey:@"slicerPath"];
                [app->slicer configSlicerAction:nil];
            } else {
                path = url.path;
                alertResult = NSRunAlertPanel(@"Slicer Not Found!", @"3DHostPrint must have an external Slicer app for slicing.", @"Retry", @"Quit",nil);
            }

        }
    }        

    if (alertResult == NSAlertDefaultReturn){ // retry
        goto retryEntry;
    }

    if (alertResult == NSAlertAlternateReturn){ // quit
        [[NSApplication sharedApplication] terminate: nil];
    }

}

- (IBAction)browseSlicerConfigFile:(id)sender {
    openPanel.message = NSLocalizedString(@"Locate Slicer configuration file (*.ini)",
                                          @"Locate Slicer configuration file (*.ini)");
    [openPanel beginSheetModalForWindow:prefWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = openPanel.URLs;
            if(urls.count>0) {
                NSURL *url = urls[0];
                [NSUserDefaults.standardUserDefaults setObject:url.path forKey:@"slicerExternalConfig"];
            }
        }        
    }];
}

- (IBAction)playPrintjobFinished:(id)sender {
    [sound playPrintjobFinished:YES];
}

- (IBAction)playPrintjobPaused:(id)sender {
    [sound playPrintjobPaused:YES];
}

- (IBAction)playError:(id)sender {
    [sound playError:YES];
}

- (IBAction)playCommand:(id)sender {
    [sound playCommand:YES];
}

- (IBAction)browsePrintjobFinished:(id)sender {
    openPanel.message = NSLocalizedString(@"Locate sound for printjob finished",
                                          @"Locate sound for printjob finished");
    [openPanel beginSheetModalForWindow:prefWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = openPanel.URLs;
            if(urls.count>0) {
                NSURL *url = urls[0];
                [NSUserDefaults.standardUserDefaults setObject:url.path forKey:@"soundPrintjobFinished"];
            }
        }        
    }];
}

- (IBAction)browsePrintjobPaused:(id)sender {
    openPanel.message = NSLocalizedString(@"Locate sound for printjob paused",
                                          @"Locate sound for printjob paused");
    [openPanel beginSheetModalForWindow:prefWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = openPanel.URLs;
            if(urls.count>0) {
                NSURL *url = urls[0];
                [NSUserDefaults.standardUserDefaults setObject:url.path forKey:@"soundPrintjobPaused"];
            }
        }        
    }];
}

- (IBAction)browseError:(id)sender {
    openPanel.message = NSLocalizedString(@"Locate sound for errors",
                                          @"Locate sound for errors");
    [openPanel beginSheetModalForWindow:prefWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = openPanel.URLs;
            if(urls.count>0) {
                NSURL *url = urls[0];
                [NSUserDefaults.standardUserDefaults setObject:url.path forKey:@"soundError"];
            }
        }        
    }];
}

- (IBAction)browseCommand:(id)sender {
    openPanel.message = NSLocalizedString(@"Locate sound for @sound command",
                                          @"Locate sound for @sound command");
    [openPanel beginSheetModalForWindow:prefWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = openPanel.URLs;
            if(urls.count>0) {
                NSURL *url = urls[0];
                [NSUserDefaults.standardUserDefaults setObject:url.path forKey:@"soundCommand"];
            }
        }        
    }];
}

@end
