/*

 Modified 2017 RSS

 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import <Foundation/Foundation.h>

@interface GCode : NSObject {
    @public
    uint16_t fields,fields2;
    int32_t n;
    uint8 t;
    uint16 g, m;
    float x, y, z, e, f,ii,j,r;
    int32_t s;
    int32_t p;
    NSString *text;
    NSString *orig;
@public
    BOOL comment;
    BOOL hostCommand;
    BOOL forceASCII;
}

-(instancetype)initFromString:(NSString*)cmd;
-(void)dealloc;

@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasM;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasN;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasG;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasT;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasX;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasY;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasZ;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasE;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasF;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasS;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasP;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasI;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasJ;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasR;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasText;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasComment;
@property (NS_NONATOMIC_IOSONLY, getter=isV2, readonly) BOOL v2;

@property (NS_NONATOMIC_IOSONLY, getter=getText, readonly, copy) NSString *text;
@property (NS_NONATOMIC_IOSONLY, getter=getG, readonly) uint16 g;
@property (NS_NONATOMIC_IOSONLY, getter=getM, readonly) uint16 m;
@property (NS_NONATOMIC_IOSONLY, getter=getT, readonly) uint8 t;
@property (NS_NONATOMIC_IOSONLY, getter=getN) int32_t n;
@property (NS_NONATOMIC_IOSONLY, getter=getS, readonly) int32_t s;
@property (NS_NONATOMIC_IOSONLY, getter=getP, readonly) int32_t p;
@property (NS_NONATOMIC_IOSONLY, getter=getX, readonly) float x;
@property (NS_NONATOMIC_IOSONLY, getter=getY, readonly) float y;
@property (NS_NONATOMIC_IOSONLY, getter=getZ, readonly) float z;
@property (NS_NONATOMIC_IOSONLY, getter=getE, readonly) float e;
@property (NS_NONATOMIC_IOSONLY, getter=getF, readonly) float f;
@property (NS_NONATOMIC_IOSONLY, getter=getI, readonly) float i;
@property (NS_NONATOMIC_IOSONLY, getter=getJ, readonly) float j;
@property (NS_NONATOMIC_IOSONLY, getter=getR, readonly) float r;
@property (NS_NONATOMIC_IOSONLY, getter=getOriginal, readonly, copy) NSString *original;
-(void)parse;
-(void) addCode:(char) c value:(NSString*)val;
-(NSString*) getAsciiWithLine:(BOOL)inclLine withChecksum:(BOOL)inclChecksum;
-(NSData*) getBinary:(int) version;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *hostCommand;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *hostParameter;
@end
