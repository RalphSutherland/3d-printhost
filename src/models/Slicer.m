/*
 Copyright 2011 repetier repetierdev@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import "Slicer.h"
#import "RHAppDelegate.h"
#import "STL.h"
#import "PrinterConfiguration.h"
#import "RHAppDelegate.h"
#import "GCodeEditorController.h"
#import "StringUtil.h"
#import "RHLogger.h"
#import "../controller/RHSlicer.h"
#import "../controller/STLComposer.h"

@implementation Slicer

-(instancetype)init {
    if((self=[super init])) {
        slicerExtConfigure = slicerExtSlice = nil;
        postprocess = nil;
        skipError = NO;
        emptyPath = [[[NSBundle mainBundle] pathForResource:@"empty" ofType:@"txt"] retain];
        NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
        NSArray *arr = @[@"slicerExternalPath", @"slicerExternalConfig", @"activeSlicer"];
        bindingsArray = [arr retain];
        for(NSString *key in arr)
            [d addObserver:self forKeyPath:key options:NSKeyValueObservingOptionNew context:NULL];
        [[NSNotificationCenter defaultCenter] addObserver:self                                             selector:@selector(taskFinished:) name:@"RHTaskFinished" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(printerChanged:) name:@"RHPrinterChanged" object:nil];
    }
    return self;
}

-(void)dealloc {
    for(NSString *key in bindingsArray)
        [NSUserDefaults.standardUserDefaults removeObserver:self
                                                 forKeyPath:key];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [bindingsArray release];
    if(slicerExtConfigure)
        [slicerExtConfigure release];
    if(slicerExtSlice)
      [slicerExtSlice release];
    [emptyPath release];

    [super dealloc];
}
-(void)awakeFromNib
{
    [self checkConfig];
}
+(BOOL)fileExists:(NSString*)fname {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir;
    BOOL exists = [fm fileExistsAtPath:fname isDirectory:&isDir];
    return exists & (!isDir);
}
// Checks configuration settings and enables/disables menus if necessary
-(void)checkConfig {
    NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
    NSString *exe = [d stringForKey:@"slicerPath"];
    [[NSApplication sharedApplication] deactivate];
    BOOL isDir;
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL fileExists = [fm fileExistsAtPath:exe isDirectory:&isDir];
    fileExists&=!isDir;
    if(!fileExists)
       return;

    (app->rhslicer.slicerActive).enabled = fileExists;
    [app->rhslicer updateSelections];

}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    [self checkConfig];
}

-(void)executePostprocess:(NSString*)file {
    if(currentPrinterConfiguration->enableFilterPrg) {
        NSString *content = [NSString stringWithContentsOfFile:file encoding:NSISOLatin1StringEncoding error:nil];
        NSString *target = [NSString stringWithFormat:@"%@postproc.gcode",rhlog.pathForLogFile];
        [content writeToFile:target atomically:YES encoding:NSISOLatin1StringEncoding error:nil];
        NSMutableArray *arr = [StringUtil explode:currentPrinterConfiguration->filterPrg sep:@" "];
        int i=0;
        if(arr.count<3) {
            [rhlog addError:NSLocalizedString(@"Postprocessing aborted. Postprocessor commands need at least 2 parameter #in and #out.",
                                              @"Postprocessing aborted. Postprocessor commands need at least 2 parameter #in and #out.")];
            [app->gcodeView loadGCode:file];
            [app->rightTabView selectTabViewItem:app->gcodeTab];
            return;
        }
        [rhlog addInfo:@"Starting postprocessor..."];
        NSString *prg = arr[0];
        [arr removeObjectAtIndex:0];
        for(NSString *s in arr) {
            s = [s stringByReplacingOccurrencesOfString:@"#in" withString:target];
            s = [s stringByReplacingOccurrencesOfString:@"#out" withString:file];
            arr[i] = s;
            i++;
        }
        postprocess = [[RHTask alloc] initProgram:prg args:arr logPrefix:@"<postproc> "];
    } else {
        [app->gcodeView loadGCodeGCode:file];
        [app->rightTabView selectTabViewItem:app->gcodeTab];
    }
}

-(void)printerChanged:(NSNotification*)event {
}

-(void)taskFinished:(NSNotification*)event {
    RHTask *t = event.object;
    if(t==postprocess) {
        if(postprocess.finishedSuccessfull) {
            [app->gcodeView loadGCodeGCode:postprocessOut];
            [app->rightTabView selectTabViewItem:app->gcodeTab];
        } else {
            [app showWarning:@"Postprocessing exited with error!" headline:@"Slicing failed"];
        }
        [postprocess release];
        [postprocessOut release];
        postprocess = nil;
        return;
    }
    if(t==slicerExtSlice) {
        if(slicerExtSlice.finishedSuccessfull) {
            [self executePostprocess:slicerExtOut];
        } else {
            if(!skipError)
                [app showWarning:@"Slicing exited with error!" headline:@"Slicing failed"];
            skipError = NO;
        }
        [slicerExtOut release];
        [slicerExtSlice release];
        slicerExtSlice = nil;
        [app->rhslicer.killButton setEnabled:NO];
    } else if(t==slicerExtConfigure) {
        [slicerExtConfigure release];
        slicerExtConfigure = nil;
    }
    [app->rhslicer updateSelections];
}

-(void)killSlicing {
    skipError = YES;
    if(slicerExtSlice!=nil)
        [slicerExtSlice kill];
    [app->rhslicer.killButton setEnabled:NO];
}

-(void) makeHostConfigFile:(NSString *) config
{

  NSString *sFilament  = currentPrinterConfiguration.slicerFilament1;
  NSString *sFilament2 = currentPrinterConfiguration.slicerFilament2;
  NSString *sFilament3 = currentPrinterConfiguration.slicerFilament3;
  NSString *sPrint     = currentPrinterConfiguration.slicerPrint;
  NSString *sPrinter   = currentPrinterConfiguration.slicerPrinter;

  NSString *cdir     = [RHSlicer slicerConfigDir];
  IniFile *ini       = [[[IniFile alloc] init] autorelease];
  NSString *fPrinter = [NSString stringWithFormat:@"%@/print/%@.ini",cdir,sPrint];
  [ini read:fPrinter];
  IniFile *ini2 = [[[IniFile alloc] init] autorelease];
  [ini2 read:[NSString stringWithFormat:@"%@/printer/%@.ini",cdir,sPrinter]];
  IniFile *ini3 = [[[IniFile alloc] init] autorelease];
  [ini3 read:[NSString stringWithFormat:@"%@/filament/%@.ini",cdir,sFilament]];
  IniFile *ini3_2 = nil;
  if(currentPrinterConfiguration->numberOfExtruder>1) {
    ini3_2 = [[[IniFile alloc] init] autorelease];
    [ini3_2 read:[NSString stringWithFormat:@"%@/filament/%@.ini",cdir,sFilament2]];
    [ini3 merge:ini3_2];
  }
  IniFile *ini3_3 = nil;
  if(currentPrinterConfiguration->numberOfExtruder>2) {
    ini3_3 = [[[IniFile alloc] init] autorelease];
    [ini3_3 read:[NSString stringWithFormat:@"%@/filament/%@.ini",cdir,sFilament3]];
    [ini3 merge:ini3_3];
  }
  [ini add:ini2];
  [ini add:ini3 ];
  [ini flatten];
  [ini write:config];

}

- (IBAction)configSlicerAction:(id)sender
{
    if(slicerExtConfigure!=nil) {
        if(slicerExtConfigure->running) {
            [slicerExtConfigure bringToFront];
            return;
        }
        [slicerExtConfigure release];
    }

    NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
    NSString *exe = [d stringForKey:@"slicerPath"];
    NSArray *arr;
    NSInteger version = [d integerForKey:@"slicerVersionGroup"];
      if(version <= 0){ // Slic3r
        arr = @[@"--gui"];
        slicerExtConfigure = [[RHTask alloc] initProgram:exe args:arr logPrefix:@"<Slic3r> "];
      }
      else { // other currently just Prusa
        arr = @[@"--ignore-nonexistent-config"];
        slicerExtConfigure = [[RHTask alloc] initProgram:exe args:arr logPrefix:@"<PrusaSlicer> "];
     }
  
  [app->rhslicer updateSelections];

}

/** Build Slicer configuration file, store stl file and start Slicer */

-(void) sliceSlicerAction:(id)sender
{

  if(slicerExtSlice!=nil) {
    if(slicerExtSlice->running) {
      return;
    }
    [slicerExtSlice release];
  }

  NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
  NSString *dir     = [d stringForKey:@"appSupportFolderPath"];
  NSString *confFile= [d stringForKey:@"appSupportConfFile"];
  NSString *stlFile = [d stringForKey:@"appSupportSTLFile"];
  NSString *exe     = [d stringForKey:@"slicerPath"];
  NSInteger version = [d integerForKey:@"slicerVersionGroup"];

  [self sliceSlicer: stlFile
           usingExe: exe version: version
              inDir: dir withConfig:confFile];

}

-(void) sliceSlicer:(NSString *)stlFile
           usingExe:(NSString*)exe version:(NSInteger) version
              inDir:(NSString*)dir withConfig:(NSString *) confFile
{

  // host temp single conf and temp stl/gcode
  NSString *config  = [dir stringByAppendingPathComponent:confFile];
  NSString *srcFile = [dir stringByAppendingPathComponent:stlFile];

  [self makeHostConfigFile:config]; // make combined slicer.ini in support area

  // copy to temp area with no spaces to work around Sli3r 1.3
  NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
  NSString *tempDir = [d stringForKey:@"appTempFolderPath"];
  NSFileManager *fileManager = [NSFileManager defaultManager];

  NSError *tempErr= nil;
  NSString *objectTempFile = [tempDir stringByAppendingPathComponent:stlFile];
  if ( [fileManager fileExistsAtPath:objectTempFile] ) {
    tempErr= nil;
    [fileManager removeItemAtPath:objectTempFile error:&tempErr];
  }
  [fileManager copyItemAtPath:srcFile
                       toPath:objectTempFile
                        error:&tempErr];

  NSString *configTempFile = [tempDir stringByAppendingPathComponent:confFile];
  if ( [fileManager fileExistsAtPath:configTempFile] ) {
    tempErr= nil;
    [fileManager removeItemAtPath:configTempFile error:&tempErr];
  }
  [fileManager copyItemAtPath:config
                       toPath:configTempFile
                        error:&tempErr];

  STL *stl = [[STL alloc] init];
  [stl load:objectTempFile];
  [stl updateBoundingBox];
    // User assigned valid position, so we use this
  double centerx = stl->xMin + (stl->xMax - stl->xMin) * 0.5;
  double centery = stl->yMin + (stl->yMax - stl->yMin) * 0.5;
  [stl release];
  slicerExtOut = [[objectTempFile.stringByDeletingPathExtension stringByAppendingString:@".gcode"] retain];

  if([Slicer fileExists:slicerExtOut]) {
    [[NSFileManager defaultManager] removeItemAtPath:slicerExtOut error:nil];
  }

  NSArray *arr; // command arguments
  if ( version <= 0 ){ // Slic3r 1.3
    arr = @[@"--load",configTempFile,
            @"--print-center",[NSString stringWithFormat:@"%d,%d",(int)centerx,(int)centery],
            @"-o",slicerExtOut,
            objectTempFile];
    slicerExtSlice = [[RHTask alloc] initProgram:exe args:arr logPrefix:@"<Slic3r> "];
  } else { // currently only Prusa
    arr = @[@"--gcode",
            @"--load",configTempFile,
            @"--center",[NSString stringWithFormat:@"%d,%d",(int)centerx,(int)centery],
            @"-o",slicerExtOut,
            objectTempFile];
    slicerExtSlice = [[RHTask alloc] initProgram:exe args:arr logPrefix:@"<PrusaSlicer> "];
  }

  [app->rhslicer.killButton setEnabled:YES];
  
}

@end
