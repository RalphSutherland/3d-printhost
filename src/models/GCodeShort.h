//
//  GCodeShort.h
//  RepetierHost
//
//  Created by Roland Littwin on 02.03.12.
//  Copyright (c) 2012 Repetier. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Stores preview relevant codes pre-parsed
 using only 24 bytes extra. This speeds up
 preview 
*/
@interface GCodeShort : NSObject {
    @public
    float x,y,z,e,f,emax,layerZ;
    // Bit 0-19 : Layer 
    // Bit 20-23 : Tool
    // Bit 24-29 : Compressed command
    // Bit 30 : Contains variables
    uint32_t flags;
    NSString *text;
}
+(GCodeShort*)codeWith:(NSString*)txt;
@property (NS_NONATOMIC_IOSONLY) int layer;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasLayer;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasVariables;
-(void)setVariables:(BOOL)val;
@property (NS_NONATOMIC_IOSONLY) int tool;
@property (NS_NONATOMIC_IOSONLY, readonly) int compressedCommand;
-(void)parse;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasX;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasY;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasZ;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasE;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL hasF;
@property (NS_NONATOMIC_IOSONLY, readonly) NSUInteger length;
-(float)getValueFor:(NSString*) key default:(float)def;
@end
