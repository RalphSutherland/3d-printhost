/*
 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import <Foundation/Foundation.h>
#import "RHTask.h"
//#import "SlicerConfig.h"
#import "../extensions/utils/IniFile.h"

@interface Slicer : NSObject {
    RHTask *slicerExtConfigure;
    RHTask *slicerExtSlice;
    RHTask *postprocess;
    NSString *postprocessOut;
    NSString *emptyPath;
    NSArray *bindingsArray;
    NSString *slicerExtOut;
    BOOL skipError;
}
-(void)checkConfig;
-(void)taskFinished:(NSNotification*)event;
-(void)printerChanged:(NSNotification*)event;
+(BOOL)fileExists:(NSString*)fname;
-(void)killSlicing;

-(IBAction)configSlicerAction:(id)sender;
-(IBAction)sliceSlicerAction:(id)sender;
-(void) sliceSlicer:(NSString *) stlFile
      usingExe: (NSString*)exe version:(NSInteger) version
      inDir:(NSString*)dir withConfig:(NSString *) confFile;

@end
