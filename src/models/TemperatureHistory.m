/*
 Copyright 2011 repetier repetierdev@googlemail.com
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "TemperatureHistory.h"
#import "DefaultsExtension.h"
#import "RHAppDelegate.h"
#import "RHTempertuareController.h"

@implementation TempertureEntry

-(instancetype)initWithExtruder:(float)ext
                            bed:(float)_bed
                      targetBed:(float)tar
                 targetExtruder:(float)tare {
    if((self=[super init])) {
        time = CFAbsoluteTimeGetCurrent();
        extruderPower = extruderAvgPower = -1000.0f;
        bedPower = bedAvgPower = -1000.0f;
        avgExtruder = avgBed   = -1000.0f;
        bed = _bed;
        extruder = ext;
        targetBed = tar;
        targetExtruder = tare;
    }
    return self;
}
-(instancetype)initWithMonitor:(int)mon temp:(float)tmp output:(float)outp targetBed:(float)tar targetExtruder:(float)tare {
    if((self=[super init])) {
        time = CFAbsoluteTimeGetCurrent();
        extruderPower = extruderAvgPower = -1000.0f;
        bedPower = bedAvgPower = -1000.0f;
        avgExtruder = avgBed   = -1000.0f;
        extruder = bed  = -1000.0f;

        //output = outp;
        //avgExtruder = bed = extruder = avgBed = avgOutput = -10000;
        targetBed = tar;
        targetExtruder = tare;
        switch(mon) {
            case 0:
            case 1:
            case 2:
            case 3:
                extruder = tmp;
                break;
            case 100:
                bed = tmp;
                break;
        }
    }
    return self;
}

@end

@implementation TemperatureList

-(instancetype)init {
    if((self = [super init])) {
        entries = [RHLinkedList new];
    }
    return self;
}
-(void)dealloc {
    [entries release];
    [super dealloc];
}
@end

@implementation TemperatureHistory

@synthesize backgroundColor;
@synthesize gridColor;
@synthesize axisColor;
@synthesize fontColor;
@synthesize extruderColor;
@synthesize avgExtruderColor;
@synthesize bedColor;
@synthesize avgBedColor;
@synthesize targetBedColor;
@synthesize targetExtruderColor;
@synthesize extPowerColor;
@synthesize extAvgPowerColor;
@synthesize bedPowerColor;
@synthesize bedAvgPowerColor;
@synthesize totPowerColor;
@synthesize totAvgPowerColor;

-(instancetype)init {
    if((self=[super init])) {        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addNotify:)
                                                     name:@"RHTempMonitor" object:nil];
        history = [TemperatureList new];
        history->maxTime = CFAbsoluteTimeGetCurrent();
        history->minTime = history->maxTime-3600;
        hourHistory = nil;
        lists = [RHLinkedList new];
        currentHour = -1;
        currentHistory = history;
        [self setupColor];
        NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
        NSArray *arr = @[@"tempBackgroundColor",
                        @"tempGridColor",@"tempAxisColor",@"tempFontColor",  
                        @"tempExtruderColor",@"tempAvgExtruderColor",
                        @"tempBedColor",@"tempAvgBedColor",
                        @"tempTargetExtruderColor",@"tempTargetBedColor",
                        @"tempTotPowerColor",@"tempTotAvgPowerColor",
                        @"tempBedPowerColor",@"tempBedAvgPowerColor",
                        @"tempExtPowerColor",@"tempExtAvgPowerColor",
                        @"tempShowExtruder",@"tempShowAverage",
                        @"tempShowBed",@"tempAutoscroll",
                        @"tempShowOutput",@"tempShowTarget",@"tempZoomLevel",
                        @"tempAverageSeconds",@"tempExtruderWidth",@"tempAvgExtruderWidth",
                        @"tempTargetExtruderWidth",@"tempBedWidth",@"tempAvgBedWidth",
                        @"tempTargetBedWidth",@"tempAvgOutputWidth"];
        bindingsArray = [arr retain];
        zoomLevel = [@[@3600.0,
                      @1800.0,
                      @900.0,@300.0,
                      @100.0,@60.0] retain];
        currentZoomLevel = 3;
        xpos = 100;
        for(NSString *key in arr)
            [d addObserver:self forKeyPath:key options:NSKeyValueObservingOptionNew context:NULL];
        
    }
    return self;
}
-(void)dealloc {
    for(NSString *key in bindingsArray)
        [NSUserDefaults.standardUserDefaults removeObserver:self
                                                 forKeyPath:key];
    [bindingsArray release];
    [zoomLevel release];
    [history release];
    [hourHistory release];
    [lists release];
    [super dealloc];
}
-(void)setupColor {
    NSUserDefaults *d = NSUserDefaults.standardUserDefaults;
    currentZoomLevel = (int)[d integerForKey:@"tempZoomLevel"];
    avgPeriod = [d integerForKey:@"tempAverageSeconds"];
    showExtruder = [d boolForKey:@"tempShowExtruder"];
    showAverage  = [d boolForKey:@"tempShowAverage"];
    showBed    = [d boolForKey:@"tempShowBed"];
    autoscoll  = [d boolForKey:@"tempAutoscroll"];
    showOutput = [d boolForKey:@"tempShowOutput"];
    showTarget = [d boolForKey:@"tempShowTarget"];
    extruderWidth = [d doubleForKey:@"tempExtruderWidth"];
    avgExtruderWidth = [d doubleForKey:@"tempAvgExtruderWidth"];
    targetExtruderWidth = [d doubleForKey:@"tempTargetExtruderWidth"];
    bedWidth    = [d doubleForKey:@"tempBedWidth"];
    avgBedWidth = [d doubleForKey:@"tempAvgBedWidth"];
    targetBedWidth = [d doubleForKey:@"tempTargetBedWidth"];
    avgOutputWidth = [d doubleForKey:@"tempAvgOutputWidth"];
    self.backgroundColor = [d colorForKey:@"tempBackgroundColor"];
    self.gridColor = [d colorForKey:@"tempGridColor"];
    self.axisColor = [d colorForKey:@"tempAxisColor"];
    self.fontColor = [d colorForKey:@"tempFontColor"];
    self.extruderColor    = [d colorForKey:@"tempExtruderColor"];
    self.avgExtruderColor = [d colorForKey:@"tempAvgExtruderColor"];
    self.bedColor    = [d colorForKey:@"tempBedColor"];
    self.avgBedColor = [d colorForKey:@"tempAvgBedColor"];
    self.targetExtruderColor = [d colorForKey:@"tempTargetExtruderColor"];
    self.targetBedColor    = [d colorForKey:@"tempTargetBedColor"];
    self.extPowerColor     = [d colorForKey:@"tempExtPowerColor"];
    self.extAvgPowerColor  = [d colorForKey:@"tempExtAvgPowerColor"];
    self.bedPowerColor     = [d colorForKey:@"tempBedPowerColor"];
    self.bedAvgPowerColor  = [d colorForKey:@"tempBedAvgPowerColor"];
    self.totPowerColor     = [d colorForKey:@"tempTotPowerColor"];
    self.totAvgPowerColor  = [d colorForKey:@"tempTotAvgPowerColor"];
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    [self setupColor];
    [app->temperatureController refresh];
}
-(void)initMenu {
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:@"Past 60 minutes" action:@selector(selectPeriod:) keyEquivalent:@""];
    item.tag = 0;
    item.target = app->temperatureController;
    [lists addLast:history];
    [app->temperatureController->timeperiodMenu addItem:item];
    [item release];
}
-(void)addNotify:(NSNotification*)event {
    TempertureEntry * ent = event.object;
    [history->entries addLast:ent];
    // Remove old entries
    double time = CFAbsoluteTimeGetCurrent();
    long ltime = (long)time;
    long lhour = ltime / 3600;
    double mintime = time-3600.0;
    while(((TempertureEntry*)(history->entries.peekFirstFast))->time<mintime){
        id e = history->entries;
        [e removeFirst];
    }
    // Create average values
    int nExtruder = 0, nBed = 0,nExtOut = 0,nBedOut = 0;
    float sumExtruder = 0.0f,sumBed = 0.0f;
    float sumExtOutput = 0.0f,sumBedOutput = 0.0f;
    mintime = CFAbsoluteTimeGetCurrent()-avgPeriod;
    for(TempertureEntry *e in history->entries) {
        if(e->time<mintime) continue;
        if(e->extruder>-1000.0f) {
            nExtruder++;
            sumExtruder+=e->extruder;
        }
        if(e->bed>-1000.0f) {
            nBed++;
            sumBed+=e->bed;
        }
        if(e->extruderPower>-1000.0f) {
            nExtOut++;
            sumExtOutput+=e->extruderPower;
        }
        if(e->bedPower>-1000.0f) {
            nBedOut++;
            sumBedOutput+=e->bedPower;
        }
    }

    if(nExtruder>0) 
        ent->avgExtruder = sumExtruder/(float)nExtruder;
    if(nBed>0) 
        ent->avgBed = sumBed/(float)nBed;

    if(nExtOut>0)
        ent->extruderAvgPower = sumExtOutput/(float)nExtOut;
    if(nBedOut>0)
        ent->bedAvgPower = sumBedOutput/(float)nBedOut;

    history->maxTime = time;
    history->minTime = time-3600;

    if(lhour != currentHour) {
        currentHour = lhour;
        NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"MMM d, H" options:0 locale:[NSLocale currentLocale]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = formatString;
        NSString *time = [dateFormatter stringFromDate:[NSDate date]];                                                         
        NSMenuItem * item = [[NSMenuItem alloc]
                             initWithTitle:time
                             action:@selector(selectPeriod:)
                             keyEquivalent:@""];
        item.target = app->temperatureController;
        item.tag = lists->count;
        hourHistory = [TemperatureList new];
        hourHistory->minTime = lhour*3600;
        hourHistory->maxTime = hourHistory->minTime+3600;
        [lists addLast:hourHistory];
        [app->temperatureController->timeperiodMenu addItem:item];
        [dateFormatter release];
        [item release];
    }
    [hourHistory->entries addLast:ent];
    [app->temperatureController refresh];
}
@end
