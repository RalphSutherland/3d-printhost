# 3DPrint Host - based on Repetier-Host Mac 0.56 with modification

The easy way to manage your 3D printer with a mac.  Remix of the great RH host, under the Apache 2.0 license.

Features include:

* Compatible with Slic3r 1.3dev and into the future.
* Improved pan and rotate of 3D views
  - simple rotation center cursor
  - readouts for location and size of objects
  - look at object button centers on selected object/s.
  - simplfied XYZ-RGB axis indicators
* Clean icons and more consistent UI redesign
* Improved Power monitor display - current extruder/bed/total power display
* No embedded slicer - requires Slic3r app as external slicer only, plays better with sandbox.
* Removed sparkle security risk!
* All new color scheme, still customisable if you disagree!
* Deprecated API replaced up to 10.8

Sadly:
* SkeinForge is gone :(

## Requirements

* Mac OS X 10.7 or higher. 10.9+ better, 10.12+ best
